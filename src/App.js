import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Hello World ReactJs
        </p>
        <a
          className="App-link"
          href="https://linkedin.com/in/wagnermor"
          target="_blank"
          rel="noopener noreferrer"
        >
          Wagner Linkedin
        </a>
      </header>
    </div>
  );
}

export default App;
